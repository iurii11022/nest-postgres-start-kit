import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { json, urlencoded } from 'express';

import { AppModule } from '#src/app.module';
import { appConfig } from '#src/common/configs';
import { LoggerService } from '#src/common/loggers/logger';

async function bootstrap() {
  const logger = new LoggerService();

  const app = await NestFactory.create(AppModule, {
    logger,
  });
  app.setGlobalPrefix(appConfig.backendPrefix);
  app.use(json({ limit: '50mb' }));
  app.use(urlencoded({ extended: true, limit: '50mb' }));
  app.enableCors({
    origin: 'http://localhost:4200',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    credentials: true,
  });

  const swaggerUrl = `${appConfig.backendPrefix}/swagger`;
  const config = new DocumentBuilder()
    .setTitle(`planner`)
    .setDescription('api service')
    .setVersion('v1')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup(swaggerUrl, app, document);

  await app.listen(appConfig.port, () => {
    logger.warn(
      `http://${appConfig.url}:${appConfig.port}/${appConfig.backendPrefix}/swagger`,
    );
    logger.warn(
      `http://${appConfig.url}:${appConfig.port}/${appConfig.backendPrefix}`,
    );
  });
}

bootstrap();
