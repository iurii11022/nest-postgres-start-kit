import {
  CallHandler,
  ExecutionContext,
  HttpException,
  InternalServerErrorException,
  NestInterceptor,
} from '@nestjs/common';
import { Request, Response } from 'express';
import type { Level } from 'pino';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { AppError } from '#src/app.error';
import { logger, setTraceId } from '#src/common/loggers/logger';

export type ExpressRequest = Request & {
  user?: any;
  userForRefreshToken?: any;
};

export class HttpLoggerInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<unknown> {
    const request: ExpressRequest = context
      .switchToHttp()
      .getRequest<Request>();

    const traceId = setTraceId(request.header('X-Request-Id'));
    context.switchToHttp().getResponse<Response>().set('X-Request-Id', traceId);

    const reqParams = {
      method: request.method,
      url: request.url,
      body: request.body,
      user:
        { id: request.user?.id, email: request.user?.email } ??
        'user is not authorized',
    };

    const startedAt = process.hrtime.bigint();

    logger.info(reqParams, `Request start`);

    return next.handle().pipe(
      catchError((err) => {
        const errorResponse = err.getResponse ? err.getResponse() : null;

        /**
         * Checks if the error is an instance of NestJS's built-in HttpException class.
         * This block manages unhandled errors, rethrows them for potential capture by an exception filter,
         * and logs them based on their status code.
         */
        if (err instanceof HttpException) {
          const status = err.getStatus();
          const level: Level =
            500 <= status && status <= 599 ? 'error' : 'debug';

          logger[level](
            { errorResponse, duration: this.fromStarted(startedAt), err },
            `an instance of NestJS's built-in HttpException`,
            `Request finished with error`,
          );

          return throwError(() => err);
        }

        /**
         * Checks if the error is an instance of the custom AppError class.
         * This block manages errors that have been explicitly handled elsewhere in the application,
         * rethrows them for potential capture by an exception filter, and prepares them for logging
         * at the appropriate level by the exception filter.
         */
        if (err instanceof AppError) {
          logger.debug(
            { errorResponse, duration: this.fromStarted(startedAt), err },
            `instance of the custom AppError`,
            `Request finished with error`,
          );
          return throwError(() => err);
        }

        /**
         * Default error handling mechanism for unanticipated error types.
         * Logs the error at the error level and wraps the original message inside
         * a new InternalServerErrorException (HTTP 500) before rethrowing.
         */
        logger.error(
          { errorResponse, duration: this.fromStarted(startedAt), err },
          `unanticipated error types`,
          `Request finished with error`,
        );
        return throwError(() => new InternalServerErrorException(err.message));
      }),
      tap((body) => {
        logger.info(
          { body, duration: this.fromStarted(startedAt) },
          `Request finished`,
        );
      }),
    );
  }

  private fromStarted(startedAt: bigint): number {
    return (
      parseFloat((process.hrtime.bigint() - startedAt).toString()) / 10 ** 9
    );
  }
}
