import { CallHandler, ExecutionContext, NestInterceptor } from '@nestjs/common';
import { Level } from 'pino';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { AppError } from '#src/app.error';
import { logger } from '#src/common/loggers/logger';

export class WsLoggerInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const client = context.switchToWs().getClient();
    const data = context.switchToWs().getData();
    const event = context.switchToWs().getPattern();

    logger.info(
      {
        user: client.data.user, // Assuming client object has an id. Adjust accordingly.
        data,
        event,
      },
      'Received WebSocket message',
    );

    const startedAt = process.hrtime.bigint();

    return next.handle().pipe(
      catchError((err) => {
        if (err instanceof AppError) {
          const status = err.status;
          const level: Level =
            500 <= status && status <= 599 ? 'error' : 'debug';

          logger[level](
            {
              duration: this.fromStarted(startedAt),
              err: err.message,
            },
            `instance of the custom WS AppError`,
            `Request finished with error`,
          );

          return throwError(() => err);
        }

        throw err; // Rethrow the error
      }),
      tap((body) => {
        logger.info(
          { body, duration: this.fromStarted(startedAt) },
          `Request finished`,
        );
      }),
    );
  }

  private fromStarted(startedAt: bigint): number {
    return (
      parseFloat((process.hrtime.bigint() - startedAt).toString()) / 10 ** 9
    );
  }
}
