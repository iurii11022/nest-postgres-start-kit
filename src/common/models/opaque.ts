declare const typeID: unique symbol;
/**
 * Allows pseudo-nominal typing in TypeScript (which uses structural typing).
 * For example, `Opaque<string, "EmployeeID">` cannot be assigned to `Opaque<string, "FirstName">`
 *
 * See https://evertpot.com/opaque-ts-types/ for detailed explanation.
 */
export type Opaque<Identifier extends string, T> = T & {
  [typeID]: Identifier;
};
