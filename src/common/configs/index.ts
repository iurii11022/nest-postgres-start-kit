import { join } from 'path';

import { getAppConfig } from '#src/common/configs/app.config';
import { getAWSConfig } from '#src/common/configs/aws.config';
import { getDbConfig } from '#src/common/configs/db.config';
import { getJwtConfig } from '#src/common/configs/jwt.config';
import { populateEnvVariables } from '#src/common/utils/function/populate-env-variables';

if (process.env.ENV_CTX === 'local') {
  populateEnvVariables(join(__dirname,   '..', '..', '..', '..', '.env'));
}

export const appConfig = getAppConfig();
export const awsConfig = getAWSConfig();
export const dbConfig = getDbConfig();
export const jwtConfig = getJwtConfig();
