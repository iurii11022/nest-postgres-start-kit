import { ensureConfigs } from '#src/common/utils/function/ensure-configs';

export type AppConfig = {
  readonly port: number;
  readonly url: string;
  readonly backendPrefix: string;
  readonly logLevel: string;
  readonly backendName: string;
  readonly version: string;
  readonly prettyLogging: boolean;
  readonly envCtx: string;
  readonly nodeEnv: string;
};

export function getAppConfig(): AppConfig {
  const configs = {
    port: process.env.BACKEND_PORT
      ? Number(process.env.BACKEND_PORT)
      : undefined,
    url: process.env.BACKEND_URL,
    backendPrefix: process.env.BACKEND_PREFIX,
    logLevel: process.env.BACKEND_LOG_LEVEL,
    backendName: process.env.BACKEND_NAME,
    version: process.env.BACKEND_VERSION,
    prettyLogging: process.env.BACKEND_PRETTY_LOGGING === 'true',
    nodeEnv: process.env.NODE_ENV,
    envCtx: process.env.ENV_CTX,
  };

  return ensureConfigs<AppConfig>(configs as AppConfig);
}
