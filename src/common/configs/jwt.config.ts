import { ensureConfigs } from '#src/common/utils/function/ensure-configs';

export type JwtConfig = {
  accessTokenSecret: string;
  accessTokenExp: number;
  refreshTokenSecret: string;
  refreshTokenExp: number;
};

export function getJwtConfig(): JwtConfig {
  const configs = {
    accessTokenSecret: process.env.JWT_ACCESS_SECRET,
    accessTokenExp: Number.isFinite(Number(process.env.JWT_ACCESS_EXP))
      ? Number(process.env.JWT_ACCESS_EXP)
      : undefined,
    refreshTokenSecret: process.env.JWT_REFRESH_SECRET,
    refreshTokenExp: Number.isFinite(Number(process.env.JWT_REFRESH_EXP))
      ? Number(process.env.JWT_REFRESH_EXP)
      : undefined,
  };

  return ensureConfigs<JwtConfig>(configs as JwtConfig);
}
