import { join } from 'path';
import { DataSource } from 'typeorm';
import { DataSourceOptions } from 'typeorm/data-source/DataSourceOptions';

import { ensureConfigs } from '#src/common/utils/function/ensure-configs';

export function getDbConfig(): DataSource {
  let dbHost = process.env.DB_HOST;

  if (process.env.USE_CLOUD_DB === 'true') {
    dbHost = process.env.CLOUD_DB_HOST;
  }

  const configs = {
    type: process.env.DB_CONNECTION as 'postgres',
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    host: dbHost,
    port: Number.isFinite(Number(process.env.DB_PORT))
      ? Number(process.env.DB_PORT)
      : undefined,
    entities: [
      join(
        __dirname,
        '..',
        '..',
        'infrastructure',
        'database',
        'entities',
        '*.entity.{ts,js}',
      ),
    ],
    synchronize: true,
    logging: false,
  };

  const ensuredConfigs = ensureConfigs<DataSourceOptions>(
    configs as DataSourceOptions,
  );

  return new DataSource({ ...ensuredConfigs });
}
