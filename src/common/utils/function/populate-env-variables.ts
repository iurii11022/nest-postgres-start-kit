import { config } from 'dotenv';

export function populateEnvVariables(path: string): void {
  const { parsed, error } = config({ path });

  if (error) {
    throw error;
  }

  if (!parsed) {
    throw new Error('Please provide env variables');
  }
}
