import { sign } from 'jsonwebtoken';

export function generateToken(
  payload: { [key: string]: string },
  secret: string,
  exp: number,
): string {
  return sign(payload, secret, { expiresIn: exp });
}
