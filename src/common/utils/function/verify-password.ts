import { UnauthorizedException } from '@nestjs/common';

import { hashPassword } from '#src/common/utils/function/hash-password';

export async function verifyPassword(
  password: string,
  hash: string,
): Promise<boolean> {
  const [salt, key] = hash.split(':');
  if (!salt || !key) {
    throw new UnauthorizedException('Invalid hash format');
  }

  const hashedPassword = await hashPassword(password, salt);
  const [_, hashedKey] = hashedPassword.split(':');

  return key === hashedKey;
}
