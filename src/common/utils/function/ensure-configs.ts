export function ensureConfigs<T>(configs: T): T {
  for (const key in configs) {
    if (
      configs[key] === '' ||
      configs[key] === undefined ||
      configs[key] === null
    ) {
      throw new Error(`Config value for "${key}" is not provided`);
    }
  }

  return configs;
}
