import * as crypto from 'crypto';
import { promisify } from 'util';

const scrypt = promisify<crypto.BinaryLike, crypto.BinaryLike, number, Buffer>(
  crypto.scrypt,
);

function generateSalt(): string {
  return crypto.randomBytes(16).toString('hex');
}

export async function hashPassword(
  password: string,
  savedSalt?: string,
): Promise<string> {
  const salt = savedSalt ? savedSalt : generateSalt();
  const derivedKey = await scrypt(password, salt, 64);
  return salt + ':' + derivedKey.toString('hex');
}
