import { LoggerService as LoggerServiceInterface } from '@nestjs/common';
import { AsyncLocalStorage } from 'async_hooks';
import { randomBytes } from 'crypto';
import Pino, { destination, Level, Logger, LoggerOptions } from 'pino';
import { PrettyOptions } from 'pino-pretty';
import { QueryRunner,Logger as TypeOrmLogger } from 'typeorm';

import { appConfig } from "#src/common/configs";

const asyncLocalStorage = new AsyncLocalStorage<string>();

export function setTraceId(requestId?: string) {
  const traceId = requestId || randomBytes(16).toString('hex');
  asyncLocalStorage.enterWith(traceId);
  return traceId;
}

const prettyConfig: PrettyOptions = {
  colorize: true,
  levelFirst: true,
  ignore: 'serviceContext',
  translateTime: 'SYS:HH:MM:ss.l',
};

const options: LoggerOptions = {
  level: appConfig.logLevel,
  base: {
    serviceContext: {
      service: appConfig.backendName,
      version: appConfig.version,
    },
  },
  mixin: () => ({ traceId: asyncLocalStorage.getStore() }),
  redact: {
    paths: ['pid', 'hostname', 'body.password'],
    remove: true,
  },
  transport: appConfig.prettyLogging
    ? {
        target: 'pino-pretty',
        options: prettyConfig,
      }
    : undefined,
};

const stdout = Pino(options);
const stderr = Pino(options, destination(2));

export const logger: Pick<Logger, Level> = {
  trace: stdout.trace.bind(stdout),
  debug: stdout.debug.bind(stdout),
  info: stdout.info.bind(stdout),
  warn: stdout.warn.bind(stdout),
  error: stderr.error.bind(stderr),
  fatal: stderr.fatal.bind(stderr),
};

export class LoggerService implements LoggerServiceInterface {
  error(message: unknown, trace?: string, context?: string) {
    logger.error({
      err: {
        message,
        stack: trace,
        context,
      },
    });
  }

  warn(message: string) {
    logger.warn(message);
  }

  log(message: string) {
    logger.info(message);
  }

  debug(message: string) {
    logger.debug(message);
  }

  verbose(message: string) {
    logger.trace(message);
  }
}

class QueryLogger implements TypeOrmLogger {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  logQuery(query: string, parameters?: unknown[], _queryRunner?: QueryRunner) {
    if (query === 'SELECT 1') return;
    logger.debug({ query, parameters }, 'New DB query');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  logQueryError(
    _error: string,
    query: string,
    parameters?: unknown[],
    _queryRunner?: QueryRunner,
  ) {
    logger.warn({ query, parameters }, 'Errored DB query');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  logQuerySlow(
    time: number,
    query: string,
    parameters?: unknown[],
    _queryRunner?: QueryRunner,
  ) {
    logger.warn({ query, parameters, time }, 'Slow DB query');
  }

  logSchemaBuild(message: string, queryRunner?: QueryRunner) {
    logger.trace(message, queryRunner);
  }

  logMigration(message: string, queryRunner?: QueryRunner) {
    logger.info(message, queryRunner);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  log(
    level: 'log' | 'info' | 'warn',
    message: unknown,
    _queryRunner?: QueryRunner,
  ) {
    switch (level) {
      case 'log':
      case 'info':
        logger.trace({ message }, 'DB log');
        break;
      case 'warn':
        logger.warn({ message }, 'DB warn');
        break;
    }
  }
}

export const queryLogger = new QueryLogger();
