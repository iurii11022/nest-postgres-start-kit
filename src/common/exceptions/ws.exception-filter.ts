import {
  ArgumentsHost,
  Catch,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { BaseWsExceptionFilter, WsException } from '@nestjs/websockets';
import { Socket } from 'socket.io';

import { AppError } from '#src/app.error';

@Catch(AppError, WsException)
export class WsExceptionFilter extends BaseWsExceptionFilter {
  catch(exception: AppError<unknown> | HttpException, host: ArgumentsHost) {
    const client: Socket = host.switchToWs().getClient();
    /**
     * Checks if the error is an instance of the custom AppError class.
     * This block handles errors that have been explicitly defined and thrown elsewhere in the
     * application and responds with the error's status and message.
     */
    if (exception instanceof AppError) {
      return this.buildResponse(client, exception.status, {
        error: 'handled error',
      });
    }

    /**
     * Default error handling mechanism for unanticipated error types.
     * If the error is neither BadRequestException nor AppError, it defaults to
     * a generic internal server error (HTTP 500) response.
     */
    return this.buildResponse(client, HttpStatus.INTERNAL_SERVER_ERROR, {
      error: exception.message ?? 'Internal server error',
    });
  }

  /**
   * Utility method to build the HTTP response based on the provided status and body.
   */
  private buildResponse(client: Socket, code: HttpStatus, body: object): any {
    client.emit('exception', {
      code,
      body,
    });

    client.disconnect();
  }
}
