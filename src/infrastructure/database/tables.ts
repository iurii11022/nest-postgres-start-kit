/**
 * This enum holds names of all tables that are created in PostgreSQL.
 */
export enum Tables {
  /** Represents companies participating in the projects. */
  USER = 'user',
}
