import { HttpStatus } from '@nestjs/common';

/**
 * AppError is an abstract base class that extends JavaScript's native Error class.
 * It serves as a foundation for specific error types within the application, adding
 * structured information and behavior to standard errors.
 *
 * @template T - Used for TypeScript's pseudo-nominal typing. Helps differentiate errors
 * and ensure they're used correctly in the context they're designed for.
 */
export abstract class AppError<T> extends Error {
  public readonly tag!: T;

  public abstract status: HttpStatus;

  protected constructor(message: string) {
    super(message);
  }
}
