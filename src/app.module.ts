import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { dbConfig } from '#src/common/configs';
import { queryLogger } from '#src/common/loggers/logger';
import { AppController } from "#src/app.controller";
import { AppService } from "#src/app.service";


@Module({
  imports: [
    TypeOrmModule.forRoot({ ...dbConfig.options, logger: queryLogger }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
