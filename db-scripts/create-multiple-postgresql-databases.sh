#!/bin/bash
# This script is using for creating multiple databases within a single PostgreSQL Server.
# Please provide env variable MULTIPLE_DATABASES with the following format:
# {username1},{password1},{db_name1};{username2},{password2},{db_name2}...

set -e
set -u

function create_user_and_database() {
	local database=$(echo $1 | tr ',' ' ' | awk  '{print $1}')
	local owner=$(echo $1 | tr ',' ' ' | awk  '{print $2}')
	local password=$(echo $1 | tr ',' ' ' | awk  '{print $3}')
	echo "  Creating user and database '$database'  & '$owner' and '$password'"
	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	    CREATE USER "$owner" WITH PASSWORD '$password';
	    CREATE DATABASE "$database";
	    GRANT ALL ON SCHEMA public TO "$owner";
	    GRANT ALL PRIVILEGES ON DATABASE "$database" TO "$owner";
EOSQL
}

if [ -n "$MULTIPLE_DATABASES" ]; then
	echo "Multiple database creation requested: $MULTIPLE_DATABASES"
	for db in $(echo $MULTIPLE_DATABASES | tr ';' ' '); do
		create_user_and_database $db
	done
	echo "Multiple databases created"
fi
