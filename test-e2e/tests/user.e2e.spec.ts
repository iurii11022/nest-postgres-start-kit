import { UserContext } from '../contexts/user.context';
import { TestAppManager } from '../setup/test-app-manager';
import { INestApplication } from '@nestjs/common';

describe('User', () => {
  let appManager = new TestAppManager();

  let app: INestApplication;
  let user: UserContext;


  beforeAll(async () => {
    app = await appManager.prepareTestApp();
    user = new UserContext(app);
    await user.init();
  });

  afterAll(() => appManager.closeApp());

  it('should return list of users', async () => {
    const response = await user.findManyUsers();

    expect(response.status).toBe(200);
    expect(Array.isArray(response.body.data)).toBeTruthy();
    expect(response.body.data.length).toBe(1);
  });
});
