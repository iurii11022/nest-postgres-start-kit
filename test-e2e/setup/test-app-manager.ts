import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { DataSource } from 'typeorm';

import { AppModule } from '#src/app.module';
import { dbConfig } from "#src/common/configs";

export class TestAppManager {
  private app: INestApplication | null = null;
  private databaseConfig: DataSource | null = null;

  async prepareTestApp(): Promise<INestApplication> {
    if (this.app) return this.app;

    this.databaseConfig = dbConfig;

    await this.databaseConfig.initialize();
    const testingModule = Test.createTestingModule({
      imports: [AppModule],
    });

    const moduleFixture = await testingModule.compile();
    this.app = await moduleFixture.createNestApplication().init();

    return this.app;
  }

  async closeApp() {
    if (this.app && this.databaseConfig) {
      await this.databaseConfig.dropDatabase();
      await this.app.close();
      this.app = null;
    }
  }
}
