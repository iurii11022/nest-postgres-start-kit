import { join } from 'path';
import { populateEnvVariables } from "#src/common/utils/function/populate-env-variables";

function prepareEnviroment() {
  populateEnvVariables(join(__dirname, '..', '..',  '.env'));
  process.env.DB_USERNAME = process.env.TEST_DB_USERNAME;
  process.env.DB_DATABASE = process.env.TEST_DB_DATABASE;
  process.env.DB_PASSWORD = process.env.TEST_DB_PASSWORD;
  process.env.ENV_CTX = 'test';
}
prepareEnviroment();
