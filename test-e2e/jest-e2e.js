module.exports = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  testEnvironment: 'node',
  rootDir: './',
  maxWorkers: 1,
  testTimeout: 3000,
  moduleNameMapper: {
    '^#src/(.*)$': '<rootDir>../src/$1',
  },
  setupFilesAfterEnv: ['./setup/jest.setup.ts'],
  moduleDirectories: ['node_modules', 'src'],
  testMatch: ['<rootDir>/tests/*.spec.ts'],
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  cache: false,
};
