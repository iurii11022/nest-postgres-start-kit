import * as request from 'supertest';
import { Response } from 'supertest';
import { INestApplication } from '@nestjs/common';
import { HttpServer } from '@nestjs/common/interfaces/http/http-server.interface';
import { io, Socket } from 'socket.io-client';
import { randomUUID } from 'crypto';
import { DefaultEventsMap } from 'socket.io/dist/typed-events';

const randomStrings = randomUUID().split('-');

export class UserContext {
  private userData  = {
    email: `${randomStrings[1]}@gmail.com`,
    name: `John${randomStrings[2]}`,
    password: `qwerty${randomStrings[3]}`,
  };
  private ioClient!: Socket<DefaultEventsMap, DefaultEventsMap>;
  private token: string | null = null;
  private readonly httpServer: HttpServer;
  private readonly wsConfig = {
    extraHeaders: {
      authorization: this.token ?? '',
      'Content-Type': 'application/json',
    },
    path: '',
    autoConnect: false,
    transports: ['websocket', 'polling'],
  };

  constructor(app: INestApplication) {
    this.httpServer = app.getHttpServer();
  }

  public async init() {
    const response = await request(this.httpServer)
      .post(`/auth/register`)
      .send(this.userData);

    this.token = response.body.access_token;
    this.wsConfig.extraHeaders.authorization = response.body.access_token;

    this.ioClient = io('http://localhost:4444', this.wsConfig);
    this.connect();
  }

  public setToken(token: string) {
    this.token = token;
    this.wsConfig.extraHeaders.authorization = token;

    this.ioClient = io('http://localhost:4444', this.wsConfig);
  }

  /**
   * methods down below related to user module
   */
  public async findManyUsers(): Promise<Response> {
    return request(this.httpServer)
      .get(`/user`)
      .set('authorization', `${this.token}`);
  }


  /**
   * methods down below related to chat and task modules
   */
  public connect(): void {
    this.ioClient.connect();
  }

  public disconnect(): void {
    this.ioClient.removeAllListeners();
    this.ioClient.disconnect();
  }

  public unsubscribe(): void {
    this.ioClient.removeAllListeners();
  }

  public emit(event: any, message: any): void {
    this.ioClient.emit(event, message);
  }

  public on(event: any | 'connect', cb: (r: any) => void): void {
    this.ioClient.on(event, cb);
  }

  public onException(event: 'exception', cb: (r: any) => void): void {
    this.ioClient.on(event, cb);
  }
}
